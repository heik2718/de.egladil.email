//=====================================================
// Projekt: de.egladil.email.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.service.impl;

import java.util.Collection;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.InvalidMailAddressException;

/**
 * @author root
 *
 */
@Singleton
public class MailserviceImpl implements IMailservice {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(MailserviceImpl.class);

	private static final String KEY_SMTP_HOST = "mail.smtp.host";

	private static final String KEY_SMTP_PORT = "mail.smtp.port";

	private static final String KEY_MAIL_USER = "mail.user";

	private static final String KEY_MAIL_PWD = "mail.password";

	private static final String KEY_MAIL_FROM = "mail.from";

	private final IEgladilConfiguration configuration;

	private MailAuthenticator mailAuthenticator;

	// private Properties systemProperties;

	private Properties mailProperties;

	/**
	 * Erzeugt eine Instanz von MailserviceImpl
	 */
	@Inject
	public MailserviceImpl(final @Named("configRoot") String pathConfigRoot) {
		configuration = new AbstractEgladilConfiguration(pathConfigRoot) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "email.properties";
			}
		};
		// if (LOG.isInfoEnabled()) {
		// configuration.printConfiguration();
		// }
		init();
	}

	void init() throws EgladilConfigurationException {
		mailAuthenticator = new MailAuthenticator(configuration.getProperty(KEY_MAIL_USER),
				configuration.getProperty(KEY_MAIL_PWD));

		mailProperties = new Properties();
		final String smtpHost = configuration.getProperty(KEY_SMTP_HOST);
		mailProperties.setProperty("mail.smtp.host", smtpHost);
		mailProperties.setProperty("mail.smtp.auth", "true");

		// SSL-Konfiguration
		mailProperties.put("mail.smtp.starttls.enable", "true");
		int port = configuration.getIntegerProperty(KEY_SMTP_PORT);
		mailProperties.put("mail.smtp.port", port);
		mailProperties.put("mail.smtp.socketFactory.port", port);
		mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		mailProperties.put("mail.smtp.socketFactory.fallback", "true");

		// systemProperties = System.getProperties();
		// final String smtpHost = configuration.getProperty(KEY_SMTP_HOST);
		// systemProperties.setProperty("mail.smtp.host", smtpHost);
		// systemProperties.setProperty("mail.smtp.auth", "true");
		//
		// // SSL-Konfiguration
		// systemProperties.put("mail.smtp.starttls.enable", "true");
		// int port = configuration.getIntegerProperty(KEY_SMTP_PORT);
		// systemProperties.put("mail.smtp.port", port);
		// systemProperties.put("mail.smtp.socketFactory.port", port);
		// systemProperties.put("mail.smtp.socketFactory.class",
		// "javax.net.ssl.SSLSocketFactory");
		// systemProperties.put("mail.smtp.socketFactory.fallback", "true");

		LOG.info("MailAuthenticator und mailProperties initialized: {}", printSMTPConfiguration());
	}

	private String printSMTPConfiguration() {
		StringBuffer sb = new StringBuffer("SMPT-Properties: ");
		for (Object key : mailProperties.keySet()) {
			sb.append(key + "=" + mailProperties.get(key));
			sb.append(", ");
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IMailservice#sendMail(de.egladil.email.service.
	 * IEmailDaten)
	 */
	@Override
	public synchronized boolean sendMail(IEmailDaten maildaten)
			throws EgladilMailException, InvalidMailAddressException {

		Session session = Session.getDefaultInstance(mailProperties, mailAuthenticator);

		if (maildaten == null) {
			throw new IllegalArgumentException("maildaten null");
		}
		final Collection<String> hiddenEmpfaenger = maildaten.getHiddenEmpfaenger();
		if (maildaten.getEmpfaenger() == null && (hiddenEmpfaenger == null || hiddenEmpfaenger.isEmpty())) {
			throw new EgladilMailException("Es muss mindestens einen Empfänger oder versteckten Empfänger geben.");
		}
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(configuration.getProperty(KEY_MAIL_FROM)));
			if (maildaten.getEmpfaenger() != null) {
				msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(maildaten.getEmpfaenger(), true));
			}
			msg.setSubject(maildaten.getBetreff(), "UTF-8");
			msg.setText(maildaten.getText(), "UTF-8");

			if (hiddenEmpfaenger != null && !hiddenEmpfaenger.isEmpty()) {
				Address[] addresses = new Address[hiddenEmpfaenger.size()];
				int index = 0;
				for (String str : hiddenEmpfaenger) {
					addresses[index] = new InternetAddress(str, true);
					index++;
				}
				msg.addRecipients(Message.RecipientType.BCC, addresses);
			} else {
				LOG.debug("HiddenEmpfaender waren null oder leer: wird ignoriert");
			}

			Transport.send(msg);
			LOG.info("Mail gesendet an {}", maildaten.getEmpfaenger());
			return true;
		} catch (SendFailedException e) {
			throw new InvalidMailAddressException(GlobalConstants.LOG_PREFIX_MAILVERSAND + "es gab ungültige Empfänger",
					e);
		} catch (MessagingException e) {
			String smtpHostPort = printSMTPConfiguration();
			String msg = "Mail an [empfaenger=" + maildaten.alleEmpfaengerFuersLog()
					+ "] konnte nicht versendet werden. " + smtpHostPort + ": " + e.getMessage();
			throw new EgladilMailException(msg, e);
		}
	}
}
