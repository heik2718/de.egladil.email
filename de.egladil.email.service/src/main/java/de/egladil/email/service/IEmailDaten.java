//=====================================================
// Projekt: de.egladil.email.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.service;

import java.util.Collection;
import java.util.List;

/**
 * @author heikew
 *
 */
public interface IEmailDaten {

	String getEmpfaenger();

	String getBetreff();

	String getText();

	Collection<String> getHiddenEmpfaenger();

	List<String> alleEmpfaengerFuersLog();
}
