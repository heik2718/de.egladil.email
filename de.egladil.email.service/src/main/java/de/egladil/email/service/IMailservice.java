//=====================================================
// Projekt: de.egladil.email.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.service;

import java.io.Serializable;

/**
 * Mailservice
 */
public interface IMailservice extends Serializable {

	/**
	 * Versendet eine Mail ohne Anhang mit den entsprechenden Daten. Die Mail wird nur dann versendet, wenn alle
	 * Adressen gültige Mailadressen sind.<br>
	 * <br>
	 * Im Falle einer InvalidMailAddressException enthält diese die Information darüber, ob die Empfänger-Adresse
	 * ungültig war. allInvalidEmpfaenger und allValidEmpfaenger enthalten alle gültigen und ungültigen Mailadressen.
	 * Damit können Logging und ExceptionHandling aussagekräftiger gestaltet werden. Außerdem kann dann bei Bedarf die
	 * Mail noch einmal an die gültigen Mailadressen gesendet werden.
	 *
	 * @param maildaten IEmailDaten darf nicht null sein.
	 * @return boolean true, wenn gesendet, false sonst.
	 * @throws EgladilMailException
	 */
	boolean sendMail(IEmailDaten maildaten) throws EgladilMailException, InvalidMailAddressException;
}
