//=====================================================
// Projekt: de.egladil.email.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.OsUtils;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.InvalidMailAddressException;

/**
 * @author root
 *
 */
public class MailserviceImplIT {

	private static final Logger LOG = LoggerFactory.getLogger(MailserviceImplIT.class);

	private MailserviceImpl mailservice;

	@BeforeEach
	public void setUp() {
		mailservice = new MailserviceImpl(OsUtils.getDevConfigRoot());
	}

	@Test
	@DisplayName("sendMail ohne hidden empfaenger")
	public void sendMailOhneHiddenEmpfaenger() throws Exception {
		// Arrange
		final IEmailDaten emailDaten = new TestEmailDaten("heike@egladil.de");

		// Act + Assert
		mailservice.sendMail(emailDaten);
	}

	@Test
	@DisplayName("sendMail ist resilient gegenueber null")
	public void sendMailHiddenEmpfaengerNull() throws Exception {
		// Arrange
		final TestEmailDaten emailDaten = new TestEmailDaten("heike@egladil.de");
		emailDaten.setHiddenEmpfaenger(null);

		// Act + Assert
		mailservice.sendMail(emailDaten);
	}

	@Test
	@DisplayName("sendMail sendet wenn 2 hidden empfaenger")
	public void shouldZweiHiddenEmpfaenger() throws Exception {
		// Arrange
		final TestEmailDaten emailDaten = new TestEmailDaten("heike@egladil.de");
		emailDaten.setHiddenEmpfaenger(
			Arrays.asList(new String[] { "hidden-empfaenger-1@egladil.de", "hidden-empfaenger-2@egladil.de" }));

		// Act + Assert
		mailservice.sendMail(emailDaten);
	}

	@Test
	@DisplayName("sendMail unterscheidet invalid account korrekt von anderen exceptions empfaenger invalid")
	public void sendMailInvalidAccountExceptionEmpfaengerInvalid() throws Exception {
		// Arrange
		final TestEmailDaten emailDaten = new TestEmailDaten("12345@67897.et");

		try {
			mailservice.sendMail(emailDaten);
			fail("keine InvalidMailAddressException");
		} catch (final InvalidMailAddressException e) {
			LOG.info(e.getMessage());
			assertTrue(e.getAllInvalidAdresses().contains(emailDaten.getEmpfaenger()));
		}
	}

	@Test
	@DisplayName("sendMail unterscheidet invalid account korrekt von anderen exceptions empfaenger valid")
	public void sendMailInvalidAccountExceptionEmpfaengerValid() throws Exception {
		// Arrange
		final TestEmailDaten emailDaten = new TestEmailDaten("heike@egladil.de");
		emailDaten.setHiddenEmpfaenger(Arrays.asList(new String[] { "hdwinkel@egladil.de", "12345@67897.et" }));

		try {
			mailservice.sendMail(emailDaten);
			fail("keine InvalidMailAddressException");
		} catch (final InvalidMailAddressException e) {
			LOG.info(e.getMessage());
			assertFalse(e.getAllInvalidAdresses().contains(emailDaten.getEmpfaenger()));
			assertFalse(e.getAllInvalidAdresses().isEmpty());
		}
	}
}
