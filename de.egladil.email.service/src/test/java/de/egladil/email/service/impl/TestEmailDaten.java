//=====================================================
// Projekt: de.egladil.email.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.egladil.email.service.IEmailDaten;

/**
 * TestEmailDaten
 */
public class TestEmailDaten implements IEmailDaten {

	private final String empfaenger;

	private final String betreff = "MailserviceImplIT";

	private final String text = "Mail aus IT";

	private List<String> hiddenEmpfaenger = new ArrayList<>();

	/**
	 * Erzeugt eine Instanz von TestEmailDaten
	 */
	public TestEmailDaten(String empfaenger) {
		this.empfaenger = empfaenger;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getEmpfaenger()
	 */
	@Override
	public String getEmpfaenger() {
		return empfaenger;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getBetreff()
	 */
	@Override
	public String getBetreff() {
		return betreff;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getText()
	 */
	@Override
	public String getText() {
		return text;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getHiddenEmpfaenger()
	 */
	@Override
	public Collection<String> getHiddenEmpfaenger() {
		return hiddenEmpfaenger;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#alleEmpfaengerFuersLog()
	 */
	@Override
	public List<String> alleEmpfaengerFuersLog() {
		List<String> result = new ArrayList<>();
		result.add(empfaenger);
		if (hiddenEmpfaenger != null) {
			result.addAll(hiddenEmpfaenger);
		}
		return result;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param hiddenEmpfaenger neuer Wert der Membervariablen hiddenEmpfaenger
	 */
	void setHiddenEmpfaenger(List<String> hiddenEmpfaenger) {
		this.hiddenEmpfaenger = hiddenEmpfaenger;
	}

}
