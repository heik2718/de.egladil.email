//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.di;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.bv.aas.storage.impl.MailqueueDaoImpl;
import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.impl.MailserviceImpl;

/**
 * MailqueueAppModule
 */
public class MailqueueAppModule extends AbstractModule {

	private final String pathConfigRoot;

	/**
	 * Erzeugt eine Instanz von MailqueueAppInjector
	 */
	public MailqueueAppModule(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());

		install(createPersistModule());
		bind(JPAInitializer.class).asEagerSingleton();

		bind(IMailservice.class).to(MailserviceImpl.class);
		bind(IMailqueueDao.class).to(MailqueueDaoImpl.class);
	}

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}

	}

	private JpaPersistModule createPersistModule() {
		final IEgladilConfiguration persistenceConfig = new AbstractEgladilConfiguration(pathConfigRoot) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "bv_persistence.properties";
			}
		};
		final JpaPersistModule jpaModule = new JpaPersistModule("bvPU");
		jpaModule.properties(persistenceConfig.getConfigurationMap());
		return jpaModule;
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
