//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.email.app.di.MailqueueAppModule;
import de.egladil.email.app.impl.TaktgeberImpl;

/**
 * Hello world!
 *
 */
public class MailqueueApp {

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueApp.class);

	private static final Pattern WINDOWS_NEWLINE = Pattern.compile("\\r\\n?");

	private static final String NAME = "Egladil Mailqueue App";

	@Parameter(names = { "-m" }, description = "Frequenz in Sekunden, in der die Tabelle Mails ausgelesen wird")
	private Integer mailqueueReadSeconds = 20;

	@Parameter(names = { "-c" }, description = "absoluter Pfad zum Konfigurationsverzeichnis", required = true)
	private String pathConfigRoot;

	@Parameter(names = { "-h", "--help" }, help = true)
	private boolean help = false;

	private MailqueueRunner mailqueueRunner;

	private TaktgeberImpl taktgeber;

	public static void main(String[] args) {
		MailqueueApp app = null;
		try {
			app = new MailqueueApp();
			app.printBanner();
			new JCommander(app, args);
			if (app.help) {
				app.printUsage();
				System.exit(0);
			}
			app.attachShutDownHook();
			app.configureAndRun();
		} catch (ParameterException e) {
			LOG.error(e.getMessage());
			if (app != null) {
				app.printUsage();
			}
			System.exit(1);
		}
	}

	/**
	 * Erzeugt eine Instanz von MailqueueApp
	 */
	public MailqueueApp() {
		LOG.info(
			"Die Mails werden im Abstand von {} Sekunde(n) abgeholt und zufällig versendet. Wartezeit liegt zwischen 3 und 180 Sekunden",
			mailqueueReadSeconds);
		taktgeber = new TaktgeberImpl();
	}

	void configureAndRun() {
		Injector injector = Guice.createInjector(new MailqueueAppModule(pathConfigRoot));
		mailqueueRunner = injector.getInstance(MailqueueRunner.class);
		mailqueueRunner.setTaktgeber(taktgeber);
		mailqueueRunner.setWartezeitMailsHolen(mailqueueReadSeconds * 1000);
		mailqueueRunner.start();
	}

	void printBanner() {
		try {
			String banner = WINDOWS_NEWLINE.matcher(loadBanner()).replaceAll("\n").replace("\n",
				String.format("%n", new Object[0]));
			LOG.info(String.format("Starting {}%n{}", new Object[0]), NAME, banner);
		} catch (IllegalArgumentException | IOException ignored) {
			LOG.info("Starting {}", NAME);
		}
	}

	private CharSequence loadBanner() throws IOException {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream("/banner.txt");
			StringWriter stringWriter = new StringWriter();
			IOUtils.copy(in, stringWriter);
			return stringWriter.toString();
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private void printUsage() {
		StringBuffer sb = new StringBuffer();
		sb.append("Usage: <main class> [options]\n");
		sb.append("   Options:\n");
		sb.append("     -h, --help\n");
		sb.append("          print usage\n");
		sb.append("     -c*\n");
		sb.append("          absoluter Pfad zum Konfigurationsverzeichnis\n");
		sb.append("     -m\n");
		sb.append("          Frequenz in Sekunden, in der die Tabelle Mails ausgelesen wird. Default ist 20\n");
		sb.append("\n");
		sb.append("Beispiel:\n");
		sb.append("   java -jar <jarname> -m 30 1\n");
		System.out.println(sb.toString());
	}

	void attachShutDownHook() {
		LOG.info("adding shutdownHook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				mailqueueRunner.shutdown();
				try {
					long waitTime = Long.valueOf(mailqueueReadSeconds) * 100;
					LOG.info("stoppe Anwendung in {} ms", "" + waitTime);
					Thread.sleep(Long.valueOf(waitTime));
				} catch (InterruptedException e) {
					// NIX
				}
				LOG.info("exit");
			}
		});
		LOG.info("shutdownHook added");
	}
}
