//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Splitter;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.email.service.IEmailDaten;

/**
 * MailqueueItemWrapper
 */
public class MailqueueItemWrapper implements IEmailDaten {

	private static final Splitter COMMA_SPLITTER = Splitter.on(",").trimResults();

	private static final String DEFAULT_EMPFAENGER = "info@egladil.de";

	private final MailqueueItem item;

	/**
	 * Erzeugt eine Instanz von MailqueueItemWrapper
	 */
	public MailqueueItemWrapper(MailqueueItem item) {
		this.item = item;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getEmpfaenger()
	 */
	@Override
	public String getEmpfaenger() {
		return null;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getBetreff()
	 */
	@Override
	public String getBetreff() {
		return item.getSubject();
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getText()
	 */
	@Override
	public String getText() {
		return item.getMailBody();
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#getHiddenEmpfaenger()
	 */
	@Override
	public Collection<String> getHiddenEmpfaenger() {
		String recipients = item.getRecipients();
		final List<String> immutable = COMMA_SPLITTER.splitToList(recipients);
		final List<String> result = new ArrayList<>();
		result.addAll(immutable);
		result.add(DEFAULT_EMPFAENGER);
		return result;
	}

	/**
	 * @see de.egladil.email.service.IEmailDaten#alleEmpfaengerFuersLog()
	 */
	@Override
	public List<String> alleEmpfaengerFuersLog() {
		List<String> result = new ArrayList<>();
		result.addAll(getHiddenEmpfaenger());
		return result;
	}

}
