//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.common.config.GlobalConstants;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.persistence.PersistenceExceptionMapper;
import de.egladil.common.persistence.utils.PrettyStringUtils;
import de.egladil.email.app.impl.Taktgeber;
import de.egladil.email.app.impl.TaktgeberImpl;
import de.egladil.email.service.EgladilMailException;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.InvalidMailAddressException;

/**
 * MailqueueThread
 */
@Singleton
public class MailqueueRunner implements Serializable {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(MailqueueRunner.class);

	private final IMailservice mailservice;

	private final IMailqueueDao mailqueueDao;

	private Taktgeber taktgeber;

	private boolean running;

	private boolean cancelled = false;

	private long wartezeitMailsHolen;

	private int anzahlMailsGesendet;

	/**
	 * Erzeugt eine Instanz von MailqueueRunner
	 */
	@Inject
	public MailqueueRunner(final IMailservice mailservice, final IMailqueueDao mailsDao) {
		this.mailservice = mailservice;
		this.mailqueueDao = mailsDao;
		this.taktgeber = new TaktgeberImpl();
	}

	/**
	 * Startet die Schleife, die nach Mails schaut.
	 *
	 * @return boolean true, falls erfolgreich angehalten.
	 */
	public void start() {
		running = true;
		while (running) {
			doTheWork();
		}
	}

	void doTheWork() {
		anzahlMailsGesendet = 0;
		final List<MailqueueItem> mailqueue = getNextMails();
		final int anzMails = mailqueue.size();
		LOG.info("neue Mails: {}", anzMails);
		if (anzMails == 0) {
			warten(wartezeitMailsHolen);
		} else {
			for (int i = 0; i < anzMails; i++) {
				final MailqueueItem mail = mailqueue.get(i);
				if (cancelled) {
					break;
				}
				if (i == 0 || i == anzMails - 1) {
					final long aktuelleWartezeit = i == 0 ? 0L : 2000;
					wartenSenden(mail, aktuelleWartezeit);
				} else {
					final long aktuelleWartezeit = taktgeber.nextSleepIntervalLengthMillis(anzMails);
					wartenSenden(mail, aktuelleWartezeit);
				}
			}
		}
	}

	@Transactional
	public List<MailqueueItem> getNextMails() {
		final List<MailqueueItem> mailqueue = mailqueueDao.findNextMails();
		return mailqueue;
	}

	void wartenSenden(final MailqueueItem mailqueueItem, final long wartezeit) {
		warten(wartezeit);
		try {
			senden(mailqueueItem);
		} catch (final InvalidMailAddressException e) {
			final List<String> allValidEmpfaenger = e.getAllValidUnsentAddresses();
			LOG.warn("Sende Mail an fehlende gültige Empfänger. Ungültige Mailadressen: {}",
				PrettyStringUtils.collectionToDefaultString(e.getAllInvalidAdresses()));
			mailqueueItem.setRecipients(PrettyStringUtils.collectionToDefaultString(allValidEmpfaenger));
			senden(mailqueueItem);
		}
	}

	/**
	 *
	 */
	private void warten(final long wartezeit) {
		try {
			LOG.debug("warte {} Sekunden", wartezeit / 1000);
			Thread.sleep(wartezeit);
		} catch (final InterruptedException e) {
			// nix tun
		}
	}

	private void senden(final MailqueueItem mailqueueItem) {
		final IEmailDaten emailDaten = new MailqueueItemWrapper(mailqueueItem);
		try {
			mailservice.sendMail(emailDaten);
			mailqueueItem.setStatus(MailqueueItemStatus.SENT);
			mailqueueItem.setStatusmessage("Mail gesendet");
			persistQuietly(mailqueueItem);
			anzahlMailsGesendet++;
			LOG.info("mail gesendet");
		} catch (final EgladilMailException e) {
			LOG.error("Fehler beim versenden der Mail {}", mailqueueItem, e);
			mailqueueItem.setStatus(MailqueueItemStatus.FAILURE);
			mailqueueItem.setStatusmessage(e.getMessage());
			persistQuietly(mailqueueItem);
		}
	}

	private void persistQuietly(final MailqueueItem item) {
		try {
			mailqueueDao.persist(item);
		} catch (final PersistenceException e) {
			final Optional<EgladilConcurrentModificationException> optConc = PersistenceExceptionMapper
				.toEgladilConcurrentModificationException(e);
			if (optConc.isPresent()) {
				LOG.warn(GlobalConstants.LOG_PREFIX_MAILVERSAND + "konkurrierendes update auf {}", item);
			}
			LOG.error("MailqueueItem konnte nicht gespeichert werden: {} - {}", e.getMessage(), item, e);
		}
	}

	/**
	 * Setzt die cancelled-Flags und gibt die letzte berechnete Wartezeit in ms zurück. So lange sollte die Anwendung
	 * mindestens warten, bis sie endgültig herunterfährt.
	 *
	 * @return
	 */
	public void shutdown() {
		this.running = false;
		this.cancelled = true;
		LOG.debug("zum Beenden vorgemerkt");
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param taktgeber neuer Wert der Membervariablen taktgeber
	 */
	public void setTaktgeber(final Taktgeber taktgeber) {
		this.taktgeber = taktgeber;
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param wartezeitMailsHolen neuer Wert der Membervariablen wartezeitMailsHolen
	 */
	public void setWartezeitMailsHolen(final long wartezeitMailsHolen) {
		this.wartezeitMailsHolen = wartezeitMailsHolen;
	}

	/**
	 * Liefert die Membervariable anzahlMailsGesendet
	 *
	 * @return die Membervariable anzahlMailsGesendet
	 */
	int getAnzahlMailsGesendet() {
		return anzahlMailsGesendet;
	}
}
