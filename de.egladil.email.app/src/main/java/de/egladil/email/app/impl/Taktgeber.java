//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.impl;

public interface Taktgeber {

	/**
	 * Gibt in Abhängigkeit von work die Länge des nächsten Intervalls zurück. Die Länge ist indirekt Proportional zu
	 * work.
	 *
	 * @return
	 */
	Long nextSleepIntervalLengthMillis(int work);
}