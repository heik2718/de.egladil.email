//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.impl;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Taktgeber
 */
public class TaktgeberImpl implements Taktgeber {

	private static final Logger LOG = LoggerFactory.getLogger(TaktgeberImpl.class);

	/**
	 * Erzeugt eine Instanz von Taktgeber
	 */
	public TaktgeberImpl() {
	}

	/**
	 * @see de.egladil.email.app.impl.Taktgeber#nextSleepIntervalLengthMillis(int)
	 */
	@Override
	public Long nextSleepIntervalLengthMillis(int work) {
		long tolerance = calculateTolerance(work);
		long avaradgeSleepInterval = tolerance / 2;
		LOG.debug("Anzahl Work: {}, avaradgeSleepInterval: {}, tolerance: {}", work, avaradgeSleepInterval, tolerance);
		long minLength = avaradgeSleepInterval - tolerance;
		if (minLength <= 3000) {
			minLength = 3000;
		}
		final long aktuelleWartezeit = RandomUtils.nextLong(minLength, avaradgeSleepInterval + tolerance);
		return aktuelleWartezeit;
	}

	long calculateTolerance(int work) {
		if (work >= 9) {
			return 10000;
		}
		if (work == 8 || work == 7) {
			return 20000;
		}
		if (work == 6 || work == 5) {
			return 30000;
		}
		if (work == 4 || work == 3) {
			return 60000;
		}
		return 120000;
	}
}
