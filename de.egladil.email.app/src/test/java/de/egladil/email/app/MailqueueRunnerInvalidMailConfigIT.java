//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import org.junit.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;

/**
 * MailqueueRunnerIT
 */
public class MailqueueRunnerInvalidMailConfigIT extends AbstractGuiceInvalidConfigIT {

	@Inject
	private MailqueueRunner runner;

	@Inject
	private IMailqueueDao mailqueueDao;

	@Test
	public void doTheWork_mit_egladil_mail_exception_klappt() {
		// Arrange
		MailqueueItem item = new MailqueueItem();
		item.setRecipients("heike@egladil.de");
		item.setMailBody("Das ist eine Testmail");
		item.setStatus(MailqueueItemStatus.WAITING);
		item.setStatusmessage("nicht gesendet");
		item.setSubject("MailqueueRunnerIT");

		mailqueueDao.persist(item);

		// Act
		runner.doTheWork();
	}
}
