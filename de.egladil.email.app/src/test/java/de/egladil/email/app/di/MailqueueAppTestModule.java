//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.di;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.common.config.OsUtils;
import de.egladil.email.service.IMailservice;
import de.egladil.email.service.impl.MailserviceImpl;

/**
 * MKVModule
 */
public class MailqueueAppTestModule extends MailqueueAppModule {

	private final String configRoot = OsUtils.getDevConfigRoot();

	/**
	 * Erzeugt eine Instanz von MKVTestModule
	 */
	public MailqueueAppTestModule(String configRoot) {
		super(configRoot);
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		super.loadProperties(binder());
		install(new BVPersistenceModule(configRoot));

		bind(IMailservice.class).to(MailserviceImpl.class);
	}

	/**
	 * @see de.egladil.mkv.service.config.MKVModule#loadProperties(com.google.inject.Binder)
	 */
	@Override
	protected void loadProperties(Binder binder) {
		Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", configRoot);
		Names.bindProperties(binder, properties);
	}
}
