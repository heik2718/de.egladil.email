//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.email.app.di.MailqueueAppTestModule;

/**
 * @author heikew
 *
 */
public abstract class AbstractGuiceIT {

	@Before
	public void setUp() {
		Injector injector = Guice.createInjector(new MailqueueAppTestModule(OsUtils.getDevConfigRoot()));
		injector.injectMembers(this);
	}
}
