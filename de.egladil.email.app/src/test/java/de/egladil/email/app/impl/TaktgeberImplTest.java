//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

import de.egladil.email.app.impl.TaktgeberImpl;

/**
 * Unit test for simple App.
 */
public class TaktgeberImplTest {

	private Random random = new Random();

	@Test
	public void test_sleepinterval_for_2() {
		// Arrange
		int work = 2;
		System.out.println("WORK=" + work);

		TaktgeberImpl taktgeber = new TaktgeberImpl();

		ArrayList<Long> results = new ArrayList<>();

		// Act
		for (int i = 0; i < 20; i++) {
			Long result = taktgeber.nextSleepIntervalLengthMillis(work);
			System.out.println("" + result / 1000 + " s");
			results.add(result);
		}

		long minLength = 3000;
		long maxLength = 180000;

		// Assert
		for (Long l : results) {
			assertTrue(l >= minLength);
			assertTrue(l <= maxLength);
		}
	}

	@Test
	public void test_sleepinterval_for_4() {
		// Arrange
		int work = 4;
		System.out.println("WORK=" + work);

		TaktgeberImpl taktgeber = new TaktgeberImpl();

		ArrayList<Long> results = new ArrayList<>();

		// Act
		for (int i = 0; i < 20; i++) {
			Long result = taktgeber.nextSleepIntervalLengthMillis(work);
			System.out.println("" + result / 1000 + " s");
			results.add(result);
		}

		long minLength = 3000;
		long maxLength = 90000;

		// Assert
		for (Long l : results) {
			assertTrue(l >= minLength);
			assertTrue(l <= maxLength);
		}
	}

	@Test
	public void test_sleepinterval_for_6() {
		// Arrange
		int work = 6;
		System.out.println("WORK=" + work);

		TaktgeberImpl taktgeber = new TaktgeberImpl();

		ArrayList<Long> results = new ArrayList<>();

		// Act
		for (int i = 0; i < 20; i++) {
			Long result = taktgeber.nextSleepIntervalLengthMillis(work);
			System.out.println("" + result / 1000 + " s");
			results.add(result);
		}

		long minLength = 3000;
		long maxLength = 45000;

		// Assert
		for (Long l : results) {
			assertTrue(l >= minLength);
			assertTrue(l <= maxLength);
		}
	}

	@Test
	public void test_sleepinterval_for_8() {
		// Arrange
		int work = 8;
		System.out.println("WORK=" + work);

		TaktgeberImpl taktgeber = new TaktgeberImpl();

		ArrayList<Long> results = new ArrayList<>();

		// Act
		for (int i = 0; i < 20; i++) {
			Long result = taktgeber.nextSleepIntervalLengthMillis(work);
			System.out.println("" + result / 1000 + " s");
			results.add(result);
		}

		long minLength = 3000;
		long maxLength = 30000;

		// Assert
		for (Long l : results) {
			assertTrue(l >= minLength);
			assertTrue(l <= maxLength);
		}
	}

	@Test
	public void test_sleepinterval_for_9() {
		// Arrange
		int work = 9;
		System.out.println("WORK=" + work);

		TaktgeberImpl taktgeber = new TaktgeberImpl();

		ArrayList<Long> results = new ArrayList<>();

		// Act
		for (int i = 0; i < 20; i++) {
			Long result = taktgeber.nextSleepIntervalLengthMillis(work);
			System.out.println("" + result / 1000 + " s");
			results.add(result);
		}

		long minLength = 3000;
		long maxLength = 15000;

		// Assert
		for (Long l : results) {
			assertTrue(l >= minLength);
			assertTrue(l <= maxLength);
		}
	}

	@Test
	public void toleranz_soll_ab_work_9_minimal_sein() {
		// Arrange
		TaktgeberImpl taktgeber = new TaktgeberImpl();
		Map<Integer, Long> ergebnisse = new HashMap<>();

		// Act
		for (int i = 0; i < 200; i++) {
			int work = random.nextInt(2000) + 9;
			long toleranz = taktgeber.calculateTolerance(work);
			ergebnisse.put(work, toleranz);
		}

		// Assert
		int anzFehler = 0;
		for (int work : ergebnisse.keySet()) {
			long toleranz = ergebnisse.get(work);
			if (toleranz != 10000L) {
				System.out.println("Fehler bei " + work + ": toleranz=" + toleranz);
				anzFehler++;
			}
		}

		assertEquals(0, anzFehler);
	}
}
