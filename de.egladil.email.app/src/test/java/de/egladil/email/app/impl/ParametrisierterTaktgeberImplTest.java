//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.egladil.email.app.impl.TaktgeberImpl;

/**
 * ParametrisierterTaktgeberTest
 */
@RunWith(Parameterized.class)
public class ParametrisierterTaktgeberImplTest {

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 0, 120000 }, { 1, 120000 }, { 2, 120000 }, { 3, 60000 }, { 4, 60000 }, { 5, 30000 },
			{ 6, 30000 }, { 7, 20000 }, { 8, 20000 }, { 9, 10000 }, { 10, 10000 } });
	}

	private int work;

	private long streuungMillis;

	private TaktgeberImpl taktgeber;

	/**
	 * Erzeugt eine Instanz von ParametrisierterTaktgeberTest
	 */
	public ParametrisierterTaktgeberImplTest(int work, long streuungMillis) {
		super();
		this.work = work;
		this.streuungMillis = streuungMillis;
		taktgeber = new TaktgeberImpl();
	}

	@Test
	public void should_streuung_be_as_expected() {
		assertEquals("Fehler bei " + work, streuungMillis, taktgeber.calculateTolerance(work));
	}

}
