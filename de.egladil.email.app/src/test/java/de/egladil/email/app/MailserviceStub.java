//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.common.config.EgladilConfigurationException;
import de.egladil.email.service.IEmailDaten;
import de.egladil.email.service.IMailservice;

/**
 * MailserviceStub
 */
public class MailserviceStub implements IMailservice {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(MailserviceStub.class);

	private boolean sendMailCalled;

	private RuntimeException exceptionToThrow;

	/**
	 * @param throwException
	 */
	public MailserviceStub() {
		sendMailCalled = false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.email.service.IMailservice#sendMail(de.egladil.email.service.IEmailDaten)
	 */
	@Override
	public boolean sendMail(IEmailDaten maildaten) throws EgladilConfigurationException {
		sendMailCalled = true;
		if (exceptionToThrow != null){
			throw exceptionToThrow;
		}
		LOG.info("sendMail called");
		return true;
	}

	/**
	 * Liefert die Membervariable sendMailCalled
	 *
	 * @return die Membervariable sendMailCalled
	 */
	public boolean isSendMailCalled() {
		return sendMailCalled;
	}

	/**
	* Setzt die Membervariable
	* @param exceptionToThrow  neuer Wert der Membervariablen exceptionToThrow
	*/
	public void setExceptionToThrow(RuntimeException exceptionToThrow) {
		this.exceptionToThrow = exceptionToThrow;
	}
}
