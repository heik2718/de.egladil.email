//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;
import de.egladil.email.app.impl.Taktgeber;

/**
 * MailqueueRunnerTest
 */
public class MailqueueRunnerTest {

	private final int work = 4;

	private IMailqueueDao mailsDao;

	private Taktgeber taktgeber;

	private MailserviceStub mailservice;

	@Test
	public void teste_algorithmus() {
		// Arrange
		mailsDao = Mockito.mock(IMailqueueDao.class);

		List<MailqueueItem> allItems = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			MailqueueItem item = new MailqueueItem();
			item.setId(Long.valueOf(i));
			item.setRecipients("hallo@egladil.de");
			item.setStatus(MailqueueItemStatus.WAITING);
			item.setSubject("irgendwas");
			item.setStatusmessage("noch nicht gesendet");
			allItems.add(item);
		}

		Mockito.when(mailsDao.findNextMails()).thenReturn(allItems);

		taktgeber = Mockito.mock(Taktgeber.class);
		mailservice = new MailserviceStub();

		Mockito.when(taktgeber.nextSleepIntervalLengthMillis(work)).thenReturn(1000L);

		MailqueueRunner runner = new MailqueueRunner(mailservice, mailsDao);
		runner.setTaktgeber(taktgeber);
		runner.setWartezeitMailsHolen(1500L);

		// Act
		runner.doTheWork();

		// Assert
		assertEquals(work, runner.getAnzahlMailsGesendet());
	}
}
