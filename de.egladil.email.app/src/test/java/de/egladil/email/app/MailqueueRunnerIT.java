//=====================================================
// Projekt: de.egladil.email.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.email.app;

import org.junit.Test;

import com.google.inject.Inject;

import de.egladil.bv.aas.domain.MailqueueItem;
import de.egladil.bv.aas.domain.MailqueueItemStatus;
import de.egladil.bv.aas.storage.IMailqueueDao;

/**
 * MailqueueRunnerIT
 */
public class MailqueueRunnerIT extends AbstractGuiceIT {

	@Inject
	private MailqueueRunner runner;

	@Inject
	private IMailqueueDao mailqueueDao;

	@Test
	public void doTheWork_ohne_mailexception_klappt() {
		// Arrange
		MailqueueItem item = new MailqueueItem();
		item.setRecipients("heike@egladil.de");
		item.setMailBody("Das ist eine Testmail");
		item.setStatus(MailqueueItemStatus.WAITING);
		item.setStatusmessage("nicht gesendet");
		item.setSubject("MailqueueRunnerIT");

		mailqueueDao.persist(item);

		// Act
		runner.doTheWork();
	}

	@Test
	public void doTheWork_mit_mailexception_klappt() {
		// Arrange
		{
			MailqueueItem item = new MailqueueItem();
			item.setRecipients("validEmpfaenger1@egladil.de");
			item.setMailBody("Das ist eine Testmail");
			item.setStatus(MailqueueItemStatus.WAITING);
			item.setStatusmessage("nicht gesendet");
			item.setSubject("MailqueueRunnerIT: validEmpfaenger1");

			mailqueueDao.persist(item);
		}
		{
			MailqueueItem item = new MailqueueItem();
			item.setRecipients("validEmpfaenger2@egladil.de, 12345@67897.et, validEmpfaenger3@egladil.de");
			item.setMailBody("validEmpfaenger2, validEmpfaenger3");
			item.setStatus(MailqueueItemStatus.WAITING);
			item.setStatusmessage("nicht gesendet");
			item.setSubject("MailqueueRunnerIT mit invalidEmpfaenger");

			mailqueueDao.persist(item);
		}
		{
			MailqueueItem item = new MailqueueItem();
			item.setRecipients("validEmpfaenger4@egladil.de");
			item.setMailBody("Das ist eine Testmail");
			item.setStatus(MailqueueItemStatus.WAITING);
			item.setStatusmessage("nicht gesendet");
			item.setSubject("MailqueueRunnerIT: validEmpfaenger4");

			mailqueueDao.persist(item);
		}

		// Act
		runner.doTheWork();
	}

}
